﻿namespace Import_Customers_WS3._0
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_username = new System.Windows.Forms.Label();
            this.lbl_password = new System.Windows.Forms.Label();
            this.lbl_server = new System.Windows.Forms.Label();
            this.cbx_serverenv = new System.Windows.Forms.ComboBox();
            this.txtb_username = new System.Windows.Forms.TextBox();
            this.txtb_password = new System.Windows.Forms.TextBox();
            this.lbl_shopid = new System.Windows.Forms.Label();
            this.txtb_shopid = new System.Windows.Forms.TextBox();
            this.lbl_store = new System.Windows.Forms.Label();
            this.btn_testws = new System.Windows.Forms.Button();
            this.statusTextBox = new System.Windows.Forms.RichTextBox();
            this.btn_openFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.fileStatusBox = new System.Windows.Forms.RichTextBox();
            this.btn_importCustomers = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.generateExamplefileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateExamplefileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repositoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_username
            // 
            this.lbl_username.AutoSize = true;
            this.lbl_username.Location = new System.Drawing.Point(20, 198);
            this.lbl_username.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_username.Name = "lbl_username";
            this.lbl_username.Size = new System.Drawing.Size(77, 17);
            this.lbl_username.TabIndex = 0;
            this.lbl_username.Text = "Username:";
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Location = new System.Drawing.Point(22, 234);
            this.lbl_password.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(73, 17);
            this.lbl_password.TabIndex = 1;
            this.lbl_password.Text = "Password:";
            // 
            // lbl_server
            // 
            this.lbl_server.AutoSize = true;
            this.lbl_server.Location = new System.Drawing.Point(12, 82);
            this.lbl_server.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_server.Name = "lbl_server";
            this.lbl_server.Size = new System.Drawing.Size(137, 17);
            this.lbl_server.TabIndex = 2;
            this.lbl_server.Text = "Server Environment:";
            // 
            // cbx_serverenv
            // 
            this.cbx_serverenv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_serverenv.FormattingEnabled = true;
            this.cbx_serverenv.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbx_serverenv.Items.AddRange(new object[] {
            "PIN",
            "IT-Gården"});
            this.cbx_serverenv.Location = new System.Drawing.Point(15, 102);
            this.cbx_serverenv.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbx_serverenv.Name = "cbx_serverenv";
            this.cbx_serverenv.Size = new System.Drawing.Size(172, 24);
            this.cbx_serverenv.TabIndex = 3;
            this.cbx_serverenv.SelectedIndexChanged += new System.EventHandler(this.cbx_serverenv_changed);
            // 
            // txtb_username
            // 
            this.txtb_username.Location = new System.Drawing.Point(105, 194);
            this.txtb_username.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtb_username.Name = "txtb_username";
            this.txtb_username.Size = new System.Drawing.Size(157, 22);
            this.txtb_username.TabIndex = 4;
            this.txtb_username.TextChanged += new System.EventHandler(this.txtb_Username_changed);
            // 
            // txtb_password
            // 
            this.txtb_password.Location = new System.Drawing.Point(105, 230);
            this.txtb_password.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtb_password.Name = "txtb_password";
            this.txtb_password.Size = new System.Drawing.Size(157, 22);
            this.txtb_password.TabIndex = 5;
            this.txtb_password.UseSystemPasswordChar = true;
            // 
            // lbl_shopid
            // 
            this.lbl_shopid.AutoSize = true;
            this.lbl_shopid.Location = new System.Drawing.Point(36, 270);
            this.lbl_shopid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_shopid.Name = "lbl_shopid";
            this.lbl_shopid.Size = new System.Drawing.Size(58, 17);
            this.lbl_shopid.TabIndex = 6;
            this.lbl_shopid.Text = "ShopID:";
            // 
            // txtb_shopid
            // 
            this.txtb_shopid.Enabled = false;
            this.txtb_shopid.Location = new System.Drawing.Point(105, 266);
            this.txtb_shopid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtb_shopid.Name = "txtb_shopid";
            this.txtb_shopid.Size = new System.Drawing.Size(157, 22);
            this.txtb_shopid.TabIndex = 7;
            // 
            // lbl_store
            // 
            this.lbl_store.AutoSize = true;
            this.lbl_store.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_store.Location = new System.Drawing.Point(15, 167);
            this.lbl_store.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_store.Name = "lbl_store";
            this.lbl_store.Size = new System.Drawing.Size(175, 24);
            this.lbl_store.TabIndex = 8;
            this.lbl_store.Text = "Store to import to:";
            // 
            // btn_testws
            // 
            this.btn_testws.Location = new System.Drawing.Point(102, 298);
            this.btn_testws.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_testws.Name = "btn_testws";
            this.btn_testws.Size = new System.Drawing.Size(158, 28);
            this.btn_testws.TabIndex = 9;
            this.btn_testws.Text = "Test Connection";
            this.btn_testws.UseVisualStyleBackColor = true;
            this.btn_testws.Click += new System.EventHandler(this.btn_testws_Click);
            // 
            // statusTextBox
            // 
            this.statusTextBox.Location = new System.Drawing.Point(360, 30);
            this.statusTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.statusTextBox.Size = new System.Drawing.Size(731, 466);
            this.statusTextBox.TabIndex = 11;
            this.statusTextBox.Text = "";
            this.statusTextBox.Click += new System.EventHandler(this.Form1_Load);
            this.statusTextBox.TextChanged += new System.EventHandler(this.Form1_Load);
            // 
            // btn_openFile
            // 
            this.btn_openFile.Enabled = false;
            this.btn_openFile.Location = new System.Drawing.Point(102, 334);
            this.btn_openFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_openFile.Name = "btn_openFile";
            this.btn_openFile.Size = new System.Drawing.Size(158, 27);
            this.btn_openFile.TabIndex = 12;
            this.btn_openFile.Text = "Open File";
            this.btn_openFile.UseVisualStyleBackColor = true;
            this.btn_openFile.Click += new System.EventHandler(this.btn_openFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 382);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 13;
            this.label1.Text = "Selected File:";
            // 
            // fileStatusBox
            // 
            this.fileStatusBox.Enabled = false;
            this.fileStatusBox.Location = new System.Drawing.Point(16, 405);
            this.fileStatusBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fileStatusBox.Name = "fileStatusBox";
            this.fileStatusBox.Size = new System.Drawing.Size(331, 22);
            this.fileStatusBox.TabIndex = 14;
            this.fileStatusBox.Text = "None....";
            // 
            // btn_importCustomers
            // 
            this.btn_importCustomers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_importCustomers.Location = new System.Drawing.Point(46, 436);
            this.btn_importCustomers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btn_importCustomers.Name = "btn_importCustomers";
            this.btn_importCustomers.Size = new System.Drawing.Size(254, 41);
            this.btn_importCustomers.TabIndex = 15;
            this.btn_importCustomers.Text = "IMPORT";
            this.btn_importCustomers.UseVisualStyleBackColor = true;
            this.btn_importCustomers.Click += new System.EventHandler(this.btn_import_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateExamplefileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1108, 28);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // generateExamplefileToolStripMenuItem
            // 
            this.generateExamplefileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateExamplefileToolStripMenuItem1});
            this.generateExamplefileToolStripMenuItem.Name = "generateExamplefileToolStripMenuItem";
            this.generateExamplefileToolStripMenuItem.Size = new System.Drawing.Size(56, 24);
            this.generateExamplefileToolStripMenuItem.Text = "Tools";
            // 
            // generateExamplefileToolStripMenuItem1
            // 
            this.generateExamplefileToolStripMenuItem1.Name = "generateExamplefileToolStripMenuItem1";
            this.generateExamplefileToolStripMenuItem1.Size = new System.Drawing.Size(226, 26);
            this.generateExamplefileToolStripMenuItem1.Text = "Generate Examplefile";
            this.generateExamplefileToolStripMenuItem1.Click += new System.EventHandler(this.generateExamplefileToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.repositoryToolStripMenuItem,
            this.versionToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // repositoryToolStripMenuItem
            // 
            this.repositoryToolStripMenuItem.Name = "repositoryToolStripMenuItem";
            this.repositoryToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.repositoryToolStripMenuItem.Text = "Repository";
            this.repositoryToolStripMenuItem.Click += new System.EventHandler(this.repositoryToolStripMenuItem_Click);
            // 
            // versionToolStripMenuItem
            // 
            this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
            this.versionToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.versionToolStripMenuItem.Text = "Version";
            this.versionToolStripMenuItem.Click += new System.EventHandler(this.versionToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1108, 512);
            this.Controls.Add(this.btn_importCustomers);
            this.Controls.Add(this.fileStatusBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_openFile);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.btn_testws);
            this.Controls.Add(this.lbl_store);
            this.Controls.Add(this.txtb_shopid);
            this.Controls.Add(this.lbl_shopid);
            this.Controls.Add(this.txtb_password);
            this.Controls.Add(this.txtb_username);
            this.Controls.Add(this.cbx_serverenv);
            this.Controls.Add(this.lbl_server);
            this.Controls.Add(this.lbl_password);
            this.Controls.Add(this.lbl_username);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import Customers Tool - WS3.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_Close);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_username;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_server;
        private System.Windows.Forms.ComboBox cbx_serverenv;
        private System.Windows.Forms.TextBox txtb_username;
        private System.Windows.Forms.TextBox txtb_password;
        private System.Windows.Forms.Label lbl_shopid;
        private System.Windows.Forms.TextBox txtb_shopid;
        private System.Windows.Forms.Label lbl_store;
        private System.Windows.Forms.Button btn_testws;
        private System.Windows.Forms.RichTextBox statusTextBox;
        private System.Windows.Forms.Button btn_openFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox fileStatusBox;
        private System.Windows.Forms.Button btn_importCustomers;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem generateExamplefileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateExamplefileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repositoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
    }
}

