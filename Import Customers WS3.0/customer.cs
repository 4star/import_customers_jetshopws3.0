﻿namespace Import_Customers_WS3._0
{
    public class customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string Zipcode { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string ExternalId { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Notes { get; set; }
        public bool HasAltAdress { get; set; }
        public string PID { get; set; }
        public string CustomerType { get; set; }
        public string PriceList { get; set; }
    }
}
