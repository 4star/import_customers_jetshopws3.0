﻿namespace Import_Customers_WS3._0
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using System.Windows.Forms.VisualStyles;
    using Microsoft.Office.Interop.Excel;
    using Microsoft.VisualBasic.FileIO;
    using MoreLinq;
    using se.jetshop.integration;
    using Application = Microsoft.Office.Interop.Excel.Application;

    public partial class Main : Form
    {
        public string WebServiceUrl { get; set; }
        public JetshopWS Webservice { get; set; }
        public Stream FileStream { get; set; }
        public List<customer> CustomersList { get; set; }
        public List<string[]> CustomersToImport { get; set; }
        private const string Path = @"C:\temp\";
        private const string Logfile = "customerImportLog.txt";
        private const string FailedImportsFile = "customerFails.txt";
        private const string ExampleFile = "JetshopCustomerImportExampleFile.xls";

        [DllImport("user32.dll")]
        static extern bool HideCaret(IntPtr hWnd);

        public Main()
        {
            CreateLogFile();
            InitializeComponent();
            InitControls();
            SetDefaultUser();
            InitializeWebService();
            LogMessage("Session started.");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StartPosition = FormStartPosition.CenterScreen;
            HideCaret(statusTextBox.Handle);
        }

        private void Form1_Close(object sender, FormClosingEventArgs e)
        {
            LogMessage("Session closed.");
        }

        #region create files

        private void CreateLogFile()
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }

            if (!File.Exists(Path + Logfile))
            {
                File.Create(Path + Logfile);
            }

            if (!File.Exists(Path + FailedImportsFile))
            {
                File.Create(Path + FailedImportsFile);
            }

            File.WriteAllText(Path + Logfile, string.Empty);
            File.WriteAllText(Path + FailedImportsFile, string.Empty);
        }

        #endregion

        #region controls

        private void repositoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("https://bitbucket.org/4star/import_customers_jetshopws3.0");
        }

        private void versionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            MessageBox.Show($"Import Customers WS3.0\nVersion: {version}");
        }

        private void DisableButtons()
        {
            btn_importCustomers.Enabled = false;
            btn_openFile.Enabled = false;
            btn_testws.Enabled = false;
        }

        private void EnableButtons()
        {
            btn_importCustomers.Enabled = true;
            btn_openFile.Enabled = true;
            btn_testws.Enabled = true;
        }

        private void InitControls()
        {
            var bs = new BindingSource {DataSource = new List<string> {"IVER", "Local"}};
            cbx_serverenv.DataSource = bs;
            cbx_serverenv.SelectedIndex = 1;
            btn_openFile.Enabled = false;
            btn_importCustomers.Enabled = false;
        }

        private void cbx_serverenv_changed(object sender, EventArgs e)
        {
            CheckSelectedServer();
            btn_openFile.Enabled = false;
            btn_importCustomers.Enabled = false;
        }

        private void txtb_Username_changed(object sender, EventArgs e)
        {
            txtb_shopid.Text = txtb_username.Text;
            btn_openFile.Enabled = false;
            btn_importCustomers.Enabled = false;
        }

        private void btn_testws_Click(object sender, EventArgs e)
        {
            btn_testws.Enabled = false;
            GetWebMethods();
        }

        private void btn_openFile_Click(object sender, EventArgs e)
        {
            btn_importCustomers.Enabled = false;
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = @"Csv files (*.csv)|*.csv";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = false;

            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;

            try
            {
                var stream = openFileDialog1.OpenFile();
                if (stream.CanRead)
                {
                    DisableButtons();
                    Cursor.Current = Cursors.WaitCursor;
                    DisplayStatusText("Reading file: " + openFileDialog1.SafeFileName);
                    fileStatusBox.Text = openFileDialog1.SafeFileName;
                    LogMessage(openFileDialog1.FileName + " opened.");
                    ReadFile(stream);
                    FileStream = stream;
                    DisplayStatusText("Checking existing customers. This might take a while depending on number of existing ones.");
                    statusTextBox.ScrollToCaret();
                    ConvertToCustomers(CustomersToImport);

                    if (CustomersList.Count > 0)
                    {
                        btn_importCustomers.Enabled = true;
                        btn_importCustomers.Focus();
                    }
                    else
                    {
                        btn_importCustomers.Enabled = false;
                    }

                    DisplayStatusText(CustomersList.Count + " customers to import.");
                    LogMessage(CustomersList.Count + " customers in " + openFileDialog1.FileName + " ready to import");
                }

                else
                {
                    DisplayStatusText("Something went wrong. Check the file.");
                }

            }
            catch (Exception ex)
            {
                DisplayStatusText((ex.Message) + "\nError logged in C:/temp/customerImportLog.txt");
                LogError(ex.Message + "\n\n" + ex.StackTrace);
            }
        }

        private void btn_import_Click(object sender, EventArgs e)
        {
            LogMessage("Starting import");
            ImportCustomers(CustomersList);
        }

        private void generateExamplefileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CreateExcelExampleFileColumns();
        }

        #endregion

        #region excel file

        private void CreateExcelExampleFileColumns()
        {
            try
            {
                if (!File.Exists(Path + ExampleFile))
                {
                    File.Create(Path + ExampleFile).Close();
                }

                var excel = new Application {Visible = false, UserControl = false};
                excel.Workbooks.Open(Path + ExampleFile, null, ReadOnly: false);

                var aWbSheet = ((Worksheet)excel.ActiveWorkbook.Sheets[1]);

                aWbSheet.Cells[1, 1] = "First Name";
                aWbSheet.Cells[1, 2] = "Last Name";
                aWbSheet.Cells[1, 3] = "Company";
                aWbSheet.Cells[1, 4] = "Phone";
                aWbSheet.Cells[1, 5] = "Mobile Phone";
                aWbSheet.Cells[1, 6] = "Zipcode";
                aWbSheet.Cells[1, 7] = "Address";
                aWbSheet.Cells[1, 8] = "City";
                aWbSheet.Cells[1, 9] = "Country";
                aWbSheet.Cells[1, 10] = "CountryCode (ie SE)";
                aWbSheet.Cells[1, 11] = "External CustomerId";
                aWbSheet.Cells[1, 12] = "Email";
                aWbSheet.Cells[1, 13] = "Username";
                aWbSheet.Cells[1, 14] = "Password";
                aWbSheet.Cells[1, 15] = "PID";
                aWbSheet.Cells[1, 16] = "CustomerType (C = Company || P = Private)";
                aWbSheet.Cells[1, 17] = "PriceList";

                PopulateExcelFileWithDummyData(aWbSheet);

                aWbSheet.Range["A1", "Q1"].Font.Bold = true;

                aWbSheet.Columns.AutoFit();

                aWbSheet.Range["A2", "P2"].NumberFormat = "@";
                aWbSheet.Range["A3", "P3"].NumberFormat = "@";
                aWbSheet.Range["A4", "P4"].NumberFormat = "@";
                aWbSheet.Columns.VerticalAlignment = VerticalAlignment.Center;
                aWbSheet.Columns.HorizontalAlignment = HorizontalAlignment.Center;

                excel.ActiveWorkbook.SaveAs(Path + ExampleFile, XlFileFormat.xlExcel8,
                    Missing.Value,
                    Missing.Value, false, false, XlSaveAsAccessMode.xlNoChange,
                    XlSaveConflictResolution.xlUserResolution, true,
                    Missing.Value, Missing.Value, Missing.Value);

                excel.ActiveWorkbook.Close(0);
                excel.Quit();

                DisplayStatusText("\nExcel file succesfully created at: " + Path + ExampleFile + "\nNote that the file needs to be imported as a comma seperated .csv file and the .xls is just to show how it should look.");
                Process.Start(Path + ExampleFile);
            }

            catch (Exception ex)
            {
                DisplayStatusText((ex.Message) + "\nError logged in C:/temp/customerImportLog.txt");
                LogError("Error when generating excel file: " + ex.Message + "\n\n" + ex.StackTrace);
            }
        }

        private static void PopulateExcelFileWithDummyData(_Worksheet sheet)
        {
            var customers = new List<customer>();

            var exCus1 = new customer
            {
                FirstName = "Tess",
                LastName = "Person",
                Company = "Företag AB",
                Phone = "+46317004242",
                MobilePhone = "+46700123456",
                Zipcode = "12345",
                Address = "Testgatan 1",
                City = "Staden",
                Country = "Sverige",
                Region = "SE",
                ExternalId = "4488",
                Email = "tess@domain.se",
                UserName = "tess@domain.se",
                Password = "changeme69",
                PID = "500002-4242",
                CustomerType = "C",
                PriceList = "standard"
            };

            var exCus2 = new customer
            {
                FirstName = "Norsk",
                LastName = "Typ",
                Company = "Lurte en ting AB",
                Phone = "+47025867687",
                MobilePhone = "+47025867687",
                Zipcode = "0001",
                Address = "Gaten",
                City = "Byn",
                Country = "Norge",
                Region = "NO",
                ExternalId = "",
                Email = "kjempefint@mailen.no",
                UserName = "kjempefint@mailen.noe",
                Password = "hurtigrutt123",
                PID = "12087749981",
                CustomerType = "P",
                PriceList = "M2"
            };

            var exCus3 = new customer
            {
                FirstName = "Tu",
                LastName = "Borg",
                Company = "Hvergang Industri",
                Phone = "+4544606060",
                MobilePhone = "+45722112233",
                Zipcode = "3700",
                Address = "Ølstræde",
                City = "Stena",
                Country = "Danmark",
                Region = "DK",
                ExternalId = "5698234",
                Email = "tu@borg.dk",
                UserName = "tu@borg.dk",
                Password = "aYhas34#dKK9we!",
                PID = "050862-0225",
                CustomerType = "C",
                PriceList = "M3"
            };

            var exCus4 = new customer
            {
                FirstName = "Allt behöver",
                LastName = "Inte fyllas i",
                Company = "Epost krävs AB",
                Phone = "",
                MobilePhone = "",
                Zipcode = "12345",
                Address = "",
                City = "",
                Country = "Sverige",
                Region = "",
                ExternalId = "",
                Email = "kund@exempel.se",
                UserName = "kund@exempel.se",
                Password = "",
                PID = "",
                CustomerType = "",
                PriceList = ""
            };

            customers.Add(exCus1);
            customers.Add(exCus2);
            customers.Add(exCus3);
            customers.Add(exCus4);

            var row = 2;

            foreach (var cus in customers)
            {
                sheet.Cells[row, 1] = cus.FirstName;
                sheet.Cells[row, 2] = cus.LastName;
                sheet.Cells[row, 3] = cus.Company;
                sheet.Cells[row, 4] = cus.Phone;
                sheet.Cells[row, 5] = cus.MobilePhone;
                sheet.Cells[row, 6] = cus.Zipcode;
                sheet.Cells[row, 7] = cus.Address;
                sheet.Cells[row, 8] = cus.City;
                sheet.Cells[row, 9] = cus.Country;
                sheet.Cells[row, 10] = cus.Region;
                sheet.Cells[row, 11] = cus.ExternalId;
                sheet.Cells[row, 12] = cus.Email;
                sheet.Cells[row, 13] = cus.Email;
                sheet.Cells[row, 14] = cus.Password;
                sheet.Cells[row, 15] = cus.PID;
                sheet.Cells[row, 16] = cus.CustomerType;
                sheet.Cells[row, 17] = cus.PriceList;

                row++;
            }
        }

        #endregion

        #region methods

        private static void LogError(string error)
        {
            var date = DateTime.Today.Date.ToString("d");
            var time = DateTime.Now.ToString("HH:mm:ss tt");
            var writer = new StreamWriter(@"C:\temp\customerImportLog.txt", true);
            writer.WriteLine(date + " " + time + " : " + error);
            writer.Close();
        }

        private static void LogFailedCustomer(string error)
        {
            var date = DateTime.Today.Date.ToString("d");
            var time = DateTime.Now.ToString("HH:mm:ss tt");
            var writer = new StreamWriter(@"C:\temp\customerFails.txt", true);
            writer.WriteLine(date + " " + time + " : " + error);
            writer.Close();
        }

        private static void LogMessage(string msg)
        {
            var date = DateTime.Today.Date.ToString("d");
            var time = DateTime.Now.ToString("HH:mm:ss tt");
            var writer = new StreamWriter(@"C:\temp\customerImportLog.txt", true);
            writer.WriteLine(date + " " + time + " : " + msg);
            writer.Close();
        }

        private void DisplayStatusText(string message)
        {
            statusTextBox.Text += message + ("\n");
            statusTextBox.SelectionStart = statusTextBox.TextLength;
            statusTextBox.ScrollToCaret();
        }

        #endregion

        #region webservice

        private void SetDefaultUser()
        {
            txtb_username.Text = config.Default.UserName;
            txtb_password.Text = config.Default.PassWord;
            txtb_shopid.Text = config.Default.UserName;
        }

        private void SetLocalUser()
        {
            this.txtb_username.Text = "debug";
            this.txtb_password.Text = "debug";
            this.txtb_shopid.Text = "debug";
        }

        private bool InitializeWebService()
        {
            DisplayStatusText("Session started.\nInitializing Jetshop Webservice");
            var win = false;
            CheckSelectedServer();
            try
            {
                var webService = new JetshopWS
                {
                    Timeout = 6000000,
                    CookieContainer = new CookieContainer(),
                    Credentials =
                        new NetworkCredential(config.Default.UserName,
                            config.Default.PassWord),
                    Url = "https://integration.jetshop.se/Webservice20/v3.0/webservice.asmx"
                };
                
                
                var getWebMethods = webService.GetCultures();
                if (getWebMethods == null)
                {
                    DisplayStatusText("Check that the username and password is correct!\nIf running local make sure WebserviceProvider and CBA is running.");
                }
                else
                {
                    LogMessage(txtb_shopid.Text + " succesfully connected to the webservice.");
                    win = true;
                }
                return win;
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                win = false;
                DisplayStatusText((ex.Message) + "\nError logged in C:/temp/customerImportLog.txt");
            }

            return win;
        }

        private string CheckSelectedServer()
        {
            WebServiceUrl = "";
            var server = cbx_serverenv.SelectedItem.ToString();
            switch (server)
            {
                case "IVER":
                    WebServiceUrl = "http://integration.jetshop.se/Webservice20/v3.0/webservice.asmx";
                    break;
                default:
                    WebServiceUrl = "http://localhost:3060/WebServiceProvider/WebService.asmx";
                    SetLocalUser();
                    break;
            }

            return WebServiceUrl;
        }

        private void GetWebMethods()
        {
            try
            {
                var webService = new JetshopWS
                {
                    Credentials =
                        new NetworkCredential(txtb_username.Text, txtb_password.Text,
                            lbl_shopid.Text), Timeout = 600000000,
                    Url = CheckSelectedServer()
                };

                var getWebMethods = webService.GetCultures();
                if (getWebMethods == null)
                {
                    DisplayStatusText("Check that the username and password is correct!");
                }
                else
                {
                    LogMessage(txtb_shopid.Text + " succesfully connected to the webservice.");
                    DisplayStatusText("Succesfully connected to the store!");
                    btn_openFile.Enabled = true;
                    Webservice = webService;
                }
            }
            catch (Exception ex)
            {
                DisplayStatusText((ex.Message) +
                                  "\nCheck that the username and password is correct! And server environment is correct. Finally check that the store has the webservice enabled\nIf running local make sure WebserviceProvider and CBA is running.");
                LogError((ex.Message) + Environment.NewLine +
                         "Check that the username and password is correct! And server environment is correct. Finally check that the store has the webservice enabled. If running local make sure WebserviceProvider and CBA is running.");
            }

            btn_testws.Enabled = true;
        }

        #endregion

        #region customer and file handling
        private void ImportCustomers(List<customer> customers)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var lastId = 0;
                var lastAddressId = 0;
                var cust = Webservice.Customer_GetAll();

                if (cust.Length > 0)
                {
                    lastId = cust.Last().CustomerId;
                    lastAddressId = cust.Last().Addresses.Last().AddressId;
                }
                else
                {
                    DisplayStatusText("No customers in database.\nAt least one is needed to create a CustomerId properly.");
                    return;
                }

                lastId++;
                lastAddressId++;
                var number = 0;
                var totalCustomers = customers.Count();
                var countryCodes = new CountryCode();

                DisableButtons();

                DisplayStatusText("Importing " + totalCustomers + " to " + this.txtb_shopid.Text);

                foreach (var c in customers)
                {
                    var add = new AddressData
                    {
                        RegistrationDate = new DateTime().Date,
                        CustomerId = lastId,
                        AddressName = "Standard",
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        Address = c.Address,
                        CountryCode = c.Region,
                        IsActive = true,
                        AddressType = 0,
                        CountryCodeIso = c.Region,
                        Company = c.Company,
                        CompanyAtt = "",
                        PostalCode = c.Zipcode,
                        City = c.City,
                        Country = c.Country,
                        Region = c.Region,
                        Phone = c.Phone,
                        MobilePhone = c.MobilePhone,
                        IsChanged = false,
                        SortOrder = 0
                    };

                    // TODO: Alternative delivery address
                    AddressData[] cusAdd = {add};
                    var cus = new CustomerData
                    {
                        CustomerType = c.CustomerType == "C" ? 1 : 0,
                        AcceptedAgeCheck = true,
                        ExternalCustomerId = c.ExternalId,
                        Email = c.Email,
                        Addresses = cusAdd,
                        CustomerId = lastId,
                        Comment = "",
                        Password = c.Password,
                        CommunicationCulture = countryCodes.GetCommunicationCulture(c.Region),
                        SocialSecurityNumber = c.PID,
                        PriceList = !string.IsNullOrWhiteSpace(c.PriceList) ? c.PriceList : "standard"
                    };

                    CustomerData[] customerData = {cus};
                    var opt = new CustomerOptions
                    {
                        CustomerId = lastId,
                        Customers = customerData
                    };

                    var exceptionOccured = false;

                    try
                    {
                        Webservice.Customer_AddUpdate(opt);
                    }
                    catch (Exception ex)
                    {
                        exceptionOccured = true;
                        DisplayStatusText($"\nCustomer with id: {lastId} edited with dummy address\nError logged in C:/temp/customerImportLog.txt\n");
                        LogError(ex.Message + "\n\n" + ex.StackTrace);
                    }

                    if (exceptionOccured)
                    {
                        AddressData[] dummyAddress = { GenerateDummyAddress(lastId, lastAddressId++) };
                        cus.Addresses = dummyAddress;
                        CustomerData[] customerDataEdited = { cus };
                        var options = new CustomerOptions
                        {
                            CustomerId = lastId,
                            Customers = customerDataEdited
                        };

                        Webservice.Customer_AddUpdate(options);
                    }

                    number++;
                    lastAddressId++;
                    lastId++;
                    statusTextBox.ScrollToCaret();
                    DisplayStatusText("Customer " + number + " of " + totalCustomers + " imported.");
                }

                EnableButtons();
                Cursor.Current = Cursors.Default;

                LogMessage(totalCustomers + " customers succesfully imported to " + txtb_shopid.Text + ".");
            }

            catch (Exception ex)
            {
                DisplayStatusText((ex.Message) + "\nError logged in C:/temp/customerImportLog.txt");
                LogError(ex.Message + "\n\n" + ex.StackTrace);
            }
        }

        private AddressData GenerateDummyAddress(int customerId, int addressId)
        {
            var dummyAddress = new AddressData
            {
                RegistrationDate = new DateTime().Date,
                AddressId = addressId,
                CustomerId = customerId,
                AddressName = "Standard",
                FirstName = "Dummy",
                LastName = "Dummy",
                Address = "Dummy",
                CountryCode = "SE",
                IsActive = true,
                AddressType = 0,
                CountryCodeIso = "SE",
                Company = "Dummy",
                CompanyAtt = "",
                PostalCode = "12345",
                City = "Dummy",
                Country = "Sverige",
                Region = "SE",
                Phone = "0700123456",
                MobilePhone = "0700123456",
                IsChanged = false,
                SortOrder = 0
            };

            return dummyAddress;
        }

        private IEnumerable<customer> ConvertToCustomers(IEnumerable<string[]> customersFromFile)
        {
            var customers =
                customersFromFile.Select(item =>
                    new customer
                    {
                        FirstName = item[0].Replace("'", ""),
                        LastName = item[1].Replace("'", ""),
                        Company = item[2].Replace("'", ""),
                        Phone = item[3].Replace("'", ""),
                        MobilePhone = item[4].Replace("'", ""),
                        Zipcode = item[5].Replace("'", ""),
                        Address = item[6].Replace("'", ""),
                        City = item[7].Replace("'", ""),
                        Country = item[8].Replace("'", ""),
                        Region = item[9].Replace("'", ""),
                        ExternalId = item[10].Replace("'", ""),
                        Email = item[11].Replace("'", ""),
                        UserName = item[12].Replace("'", ""),
                        Password = item[13].Replace("'", ""),
                        PID = item[14].Replace("'", ""),
                        CustomerType = item[15].Replace("'", ""),
                        PriceList = item[16].Replace("'", "")
                    }).ToList();
            
            var cd = customers.DistinctBy(cust => cust.Email.ToLower());
            var customersToCheck = cd as IList<customer> ?? cd.ToList();
            var fixedCollection = customersToCheck.ToList();

            var countries = Webservice.GetCultures().Select(l => l.Culture).ToList();
            var cc = new CountryCode();

            foreach (var item in customersToCheck)
            {
                if (string.IsNullOrWhiteSpace(item.Region) || !countries.Contains(cc.GetCommunicationCulture(item.Region)))
                {
                    item.Region = "SE";
                }


                if (item.City.Any(char.IsDigit))
                {
                    fixedCollection.Remove(item);
                    LogFailedCustomer(item.Email + " not imported due to digits in city");
                }

                var isValidMail =
                    Regex.IsMatch(item.Email,
                        @"^[A-Za-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$");
                if (!isValidMail)
                {
                    fixedCollection.Remove(item);
                    LogFailedCustomer(item.Email + " not imported due to not valid email");
                }

                if (!IsNullOrWhiteSpace(item.Email)) continue;
                fixedCollection.Remove(item);
                LogFailedCustomer(item.Email + " not imported due to empty email");
            }

            var cd2 = fixedCollection.DistinctBy(cust => cust.Email.ToLower());
            var customersListTemp = cd2.ToList();
            CustomersList = customersListTemp;

            var custall = Webservice.Customer_GetAll();

            foreach (var c in custall.ToList().Select(c => c.Email))
            {
                var x = CustomersList.Any(a => a.Email.Equals(c));
                if (!x) continue;
                var cus = customersListTemp.Find(o => o.Email.Equals(c));
                CustomersList.Remove(cus);
                LogFailedCustomer(cus.Email + " not imported due to duplicated mail");
            }

            EnableButtons();
            Cursor.Current = Cursors.Default;

            return CustomersList;
        }

        public static bool IsNullOrWhiteSpace(string value)
        {
            return value == null || value.All(char.IsWhiteSpace);
        }

        private IEnumerable<string[]> ReadFile(Stream filestream)
        {
            var lines = new List<string[]>();

            try
            {
                using (var parser = new TextFieldParser(filestream, Encoding.GetEncoding(1252), true))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",",";");

                    while (!parser.EndOfData)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        var fields = parser.ReadFields();
                        if (fields == null) continue;
                        lines.Add(fields);
                    }
                    lines.RemoveAt(0);
                    CustomersToImport = lines;
                }
            }
            catch (Exception ex)
            {
                DisplayStatusText((ex.Message) + "\nError logged in C:/temp/customerImportLog.txt");
                LogError(ex.Message + "\n\n" + ex.StackTrace);
            }
            return CustomersToImport;
        }

        #endregion
    }
}
