﻿namespace Import_Customers_WS3._0
{
    using System.Collections.Generic;

    public class CountryCode
    {
        private Dictionary<string, string> CountryCodes { get; set; }

        public CountryCode()
        {
            this.CountryCodes = new Dictionary<string, string>();
            CountryCodes.Add("SE", "sv-SE");
            CountryCodes.Add("EN", "en-GB");
            CountryCodes.Add("GB", "en-GB");
            CountryCodes.Add("UK", "en-GB");
            CountryCodes.Add("NO", "nb-NO");
            CountryCodes.Add("US", "en-US");
            CountryCodes.Add("DK", "da-DK");
            CountryCodes.Add("FI", "fi-FI");
            CountryCodes.Add("DE", "de-DE");
            CountryCodes.Add("GLOBAL", "en-GB");
        }

        public string GetCommunicationCulture(string culture)
        {
            var communicationCulture = "";
            var cc = new CountryCode();

            foreach (var key in cc.CountryCodes)
            {
                if (string.IsNullOrWhiteSpace(culture) || culture != key.Key) continue;
                communicationCulture = key.Value;
                return communicationCulture;
            }

            communicationCulture = "sv-SE";
            return communicationCulture;
        }
    }
}
