# README #

### Quick summary ###

* A program that lets one import customers from a .csv file to a Jetshop Commerce store (68.1)
* Log files are stored in C:\temp

## How do I get set up? ##

### Summary of set up ###

* Go to [http://portal.office.jetshop.se/integration](Link URL) and find the store (must have WS enabled).
* Enter the stores username and password and click 'connect'

### Configuration ###

The fields can differ from file to file and either file must be changed to match order in ConvertToCustomers() method or the method needs to be edited.

### Dependencies: ###

* Internet connection
* A store with Jetshop 3.0 Webservice enabled (credentials)
* A proper .csv file

### Who do I talk to? ###

Daniel M�ller

### Other stuff ###
* Store needs minimum of 1 customer to properly set CustomerID
* tbl_customers might have to be re-seeded, ie there is one customer with id 1000. Then DBCC CHECKIDENT ('tbl_customers',RESEED,1000) is needed before import
* If import is messed up:

	DELETE FROM tbl_Customers WHERE CustomerID > X

	DELETE FROM tbl_Addresses WHERE CustomerID > X

	DELETE FROM tbl_CustomersInCustomerGroups WHERE CustomerID > X

	DELETE FROM tbl_CustomersInPriceLists WHERE CustomerID > X

	And X is customerid where it all went wrong